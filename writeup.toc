\contentsline {chapter}{فهرست جداول}{iv}{chapter*.2}
\contentsline {chapter}{فهرست تصاویر}{v}{chapter*.3}
\contentsline {chapter}{\numberline {فصل\nobreakspace {}اول}معرفی بسته نرم‌افزاری {\texttt {HERAFitter}}\xspace }{1}{chapter.1}
\contentsline {section}{\numberline {1-1}معرفی}{1}{section.1.1}
\contentsline {section}{\numberline {1-2}تئوری ورودی}{2}{section.1.2}
\contentsline {subsection}{\numberline {1-2-1}فرمالیته پراکندگی ناکشسان ژرف و رهیافت‌ها}{3}{subsection.1.2.1}
\contentsline {subsubsection}{رهیافت تعداد طعم متغیر بدون جرم}{7}{subsubsection*.4}
\contentsline {subsubsection}{رهیافت طعم متغیر با جرم کلی : رهیافت \lr {Throne-Robert}}{7}{subsubsection*.5}
\contentsline {subsubsection}{رهیافت طعم متغیر با جرم کلی : رهیافت \lr {ACOT}}{8}{subsubsection*.6}
\contentsline {subsubsection}{رهیافت تعداد طعم ثابت}{9}{subsubsection*.7}
\contentsline {subsubsection}{تصحیحات الکتروضعیف برای پراکندگی $ ep $}{9}{subsubsection*.8}
\contentsline {subsection}{\numberline {1-2-2}فرآیندهای \lr {Drell-Yan}}{10}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1-2-3}سطح‌مقطع‌های تولید $t\overline {t}$ در برخوردهای $pp$ یا $p\overline {p}$}{13}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1-2-4}جت‌ها}{13}{subsection.1.2.4}
\contentsline {subsubsection}{\lr {fastNLO}}{14}{subsubsection*.9}
\contentsline {subsubsection}{\lr {APPLGRID}}{14}{subsubsection*.10}
\contentsline {subsection}{\numberline {1-2-5}مدل‌های دوقطبی}{15}{subsection.1.2.5}
\contentsline {subsubsection}{مدل \lr {GBW}}{16}{subsubsection*.11}
\contentsline {subsubsection}{مدل \lr {IIM}}{17}{subsubsection*.12}
\contentsline {subsubsection}{مدل \lr {BGK}}{17}{subsubsection*.13}
\contentsline {subsubsection}{مدل \lr {BGK} با کوارک‌های ظرفیت}{17}{subsubsection*.14}
\contentsline {subsection}{\numberline {1-2-6}تکانه عرضی وابسته (\lr {PDF} غیریکپارچه) با \lr {CCFM}}{18}{subsection.1.2.6}
\contentsline {subsection}{\numberline {1-2-7}\lr {PDF} های پراش}{19}{subsection.1.2.7}
\contentsline {subsubsection}{سطح مقطع}{20}{subsubsection*.15}
\contentsline {subsubsection}{عامل‌بندی \lr {Regge}}{21}{subsubsection*.16}
\contentsline {section}{\numberline {1-3}اصول برازش \lr {PDF} ها}{22}{section.1.3}
\contentsline {subsection}{\numberline {1-3-1}پارامتربندی \lr {pdf} ها }{23}{subsection.1.3.1}
\contentsline {subsubsection}{شکل توابع استاندارد}{23}{subsubsection*.17}
\contentsline {subsubsection}{فرم تابعی \lr {Bi-Log-Normal}}{25}{subsubsection*.18}
\contentsline {subsubsection}{فرم تابعی چندجمله‌ای چبیشف}{25}{subsubsection*.19}
\contentsline {subsubsection}{شکل تابعی پارامتربندی پراش}{26}{subsubsection*.20}
\contentsline {subsection}{\numberline {1-3-2}تعریف تابع \lr {Chisquare} $\chi ^2$}{28}{subsection.1.3.2}
\contentsline {subsubsection}{با استفاده از پارامترهای اضافه}{28}{subsubsection*.21}
\contentsline {subsubsection}{نحوه اجرا در {\texttt {HERAFitter}}\xspace }{31}{subsubsection*.22}
\contentsline {subsection}{\numberline {1-3-3}رفتار عدم قطعیت تجربی}{33}{subsection.1.3.3}
\contentsline {subsubsection}{خظاهای همبسته و $\chi ^2$}{33}{subsubsection*.23}
\contentsline {subsubsection}{روش آفست}{35}{subsubsection*.24}
\contentsline {subsubsection}{روش مونت کارلو}{37}{subsubsection*.25}
\contentsline {subsubsection}{نحوه اجرا در {\texttt {HERAFitter}}\xspace }{38}{subsubsection*.26}
\contentsline {subsubsection}{روش‌های تنظیم}{38}{subsubsection*.27}
\contentsline {section}{\numberline {1-4}تکنیک توزین Bayesian}{41}{section.1.4}
\contentsline {subsection}{\numberline {1-4-1}توزیع‌های احتمال PDF }{41}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1-4-2}توزین Bayesian مجموعه‌های PDF}{43}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1-4-3}نحوه استفاده توزین PDF در برنامه {\texttt {HERAFitter}}\xspace }{44}{subsection.1.4.3}
\contentsline {section}{\numberline {1-5}راهنمای برنامه}{46}{section.1.5}
\contentsline {subsection}{\numberline {1-5-1}دستورالعمل‌های نصب برنامه}{46}{subsection.1.5.1}
\contentsline {subsubsection}{پیش‌نیازها}{46}{subsubsection*.28}
\contentsline {subsubsection}{نصب پیش‌فرض}{46}{subsubsection*.29}
\contentsline {subsubsection}{نصب به همراه {\texttt {APPL\-GRID}}\xspace }{47}{subsubsection*.30}
\contentsline {subsubsection}{نصب به همراه {\texttt {LHAPDF}}\xspace }{48}{subsubsection*.31}
\contentsline {subsection}{\numberline {1-5-2}نصب به همراه توزین PDF}{48}{subsection.1.5.2}
\contentsline {subsubsection}{نصب به همراه {\tt HATHOR}}{49}{subsubsection*.32}
\contentsline {subsubsection}{ نصب برای TMD (uPDF) در عامل‌بندی انرژی بالا (با استفاده از {\tt CASCADE} ) }{49}{subsubsection*.33}
\contentsline {subsection}{\numberline {1-5-3}راهنمای کاربر}{51}{subsection.1.5.3}
\contentsline {subsubsection}{سازمان کد}{51}{subsubsection*.34}
\contentsline {subsubsection}{فایل‌های فرمان}{52}{subsubsection*.35}
\contentsline {subsubsection}{قالب فایل‌های داده}{61}{subsubsection*.36}
\contentsline {subsubsection}{درک خروجی}{66}{subsubsection*.37}
\contentsline {subsection}{\numberline {1-5-4}مثال کاربر}{68}{subsection.1.5.4}
\contentsline {subsubsection}{داده های فراگیر DIS}{68}{subsubsection*.38}
\contentsline {subsubsection}{همه واکنش‌ها}{68}{subsubsection*.39}
\contentsline {subsubsection}{ چگونه داده جدید اضافه کنیم}{68}{subsubsection*.40}
\contentsline {chapter}{\numberline {فصل\nobreakspace {}دوم}مدل‌های مختلف پارامتربندی}{70}{chapter.2}
\contentsline {section}{\numberline {2-1}چندجمله‌ای‌های استاندارد}{70}{section.2.1}
\contentsline {section}{\numberline {2-2}توزیع‌های bi-log-normal}{71}{section.2.2}
\contentsline {section}{\numberline {2-3}چندجمله‌ای‌های چبیشف}{73}{section.2.3}
\contentsline {section}{\numberline {2-4}PDF های خارجی}{73}{section.2.4}
\contentsline {section}{\numberline {2-5}نتایج برازش}{75}{section.2.5}
\contentsline {chapter}{\numberline {فصل\nobreakspace {}سوم}برازش با شکل جدید پارامتری}{78}{chapter.3}
\contentsline {section}{\numberline {3-1}مقدمه}{78}{section.3.1}
\contentsline {section}{\numberline {3-2}چارچوب نظری}{79}{section.3.2}
\contentsline {subsection}{\numberline {3-2-1}شکل پارامتری}{79}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3-2-2}کوارک‌های سنگین}{79}{subsection.3.2.2}
\contentsline {section}{\numberline {3-3}داده‌ها}{80}{section.3.3}
\contentsline {section}{\numberline {3-4}نتایج}{80}{section.3.4}
\contentsline {section}{\numberline {3-5}نتیجه}{85}{section.3.5}
\contentsline {chapter}{{\rl {مراجع}\hfill }}{86}{appendix*.41}
